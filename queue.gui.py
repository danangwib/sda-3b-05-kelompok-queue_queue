from tkinter import *


antrian = []
namapasien = []
alamatpasien = []


def nomorurut():
    with open("store.txt", 'r') as f:
        a = f.readlines()  # read from file to variable a
    b = int(a[0])  # get integer at first position
    b = b + 1  # increment
    with open("store.txt", 'w') as f:  # open same file
        f.write(str(b))  # writing a assuming it has been changed
    return b


def clear_frame():
    for widgets in frame.winfo_children():
        widgets.destroy()


def menudaftar():
    clear_frame()
    nama_label = Label(frame, text="NAMA PASIEN", font=('Lucida Sans', 12, 'bold'), fg="black", bg="#F3E2A9")
    nama_label.place(relx=0.24, rely=0.4, anchor=CENTER)

    nama_entry = Entry(frame, width=25, bg="white")
    nama_entry.place(relx=0.8, rely=0.4, anchor=CENTER)
    namapasien.append(nama_entry)

    next = Button(frame, text="Selanjutnya", bg="#FACC2E", height="1", width="13", command=menujualamat)
    next.place(relx=0.5, rely=0.5, anchor=CENTER)


def menujualamat():
    clear_frame()
    alamat_label = Label(frame, text="ALAMAT", font=('Lucida Sans', 12, 'bold'), fg="black", bg="#F3E2A9")
    alamat_label.place(relx=0.2, rely=0.4, anchor=CENTER)

    alamat_entry = Entry(frame, width=25, bg="white")
    alamat_entry.place(relx=0.8, rely=0.4, anchor=CENTER)

    alamatpasien.append(alamat_entry)

    next2 = Button(frame, text="Tambahkan", bg="#FACC2E", height="1", width="13",  command=tambahkan)
    next2.place(relx=0.5, rely=0.5, anchor=CENTER)


def tambahkan():
    clear_frame()
    nomorantrian = nomorurut()
    antrian.append(nomorantrian)
    thanks = Label(frame,
                   text="TERIMA KASIH\n NOMOR ANTRIAN ANDA\n {}".format(nomorantrian),
                   font=('Lucida Sans', 13, 'bold'), bg="#F3E2A9")
    thanks.place(relx=0.5, rely=0.5, anchor=CENTER)
    selesai = Button(frame, text="Selesai", bg="#FACC2E", height="1", width="13", command=menu)
    selesai.place(relx=0.5, rely=0.65, anchor=CENTER)


def panggilpasien():
    if len(antrian) == 0:
        clear_frame()

        antrian_kosong = Label(frame, text="MAAF\n ANTRIAN KOSONG!", font=('Lucida Sans', 15, 'bold'), bg="#F3E2A9")
        antrian_kosong.place(relx=0.5, rely=0.4, anchor=CENTER)

        done = Button(frame, text="Selesai", bg="#FACC2E",  height="1", width="13", command=menu)
        done.place(relx=0.5, rely=0.5, anchor=CENTER)

    else:
        clear_frame()

        antrian_panggil = Label(frame, text="PANGGILAN", font=('Lucida Sans', 15, 'bold'), bg="#F3E2A9")
        antrian_panggil.place(relx=0.5, rely=0.4, anchor=CENTER)

        panggil = Label(frame, text="NOMOR ANTRIAN", font=('Lucida Sans', 15, 'bold'), bg="#F3E2A9")
        panggil.place(relx=0.5, rely=0.5, anchor=CENTER)

        nomor_antrian = Label(frame, text="{}".format(antrian[0]), font=('Lucida Sans', 14), bg="#F3E2A9")
        nomor_antrian.place(relx=0.5, rely=0.6, anchor=CENTER)

        # tks = Label(frame, text="Terima Kasih Semoga harimu menyenangkan", font="8", bg="#F3E2A9")
        # tks.place(relx=0.5, rely=0.7, anchor=CENTER)

        done = Button(frame, text="Selesai", bg="#FACC2E", height="1", width="13", command=menu)
        done.place(relx=0.5, rely=0.7, anchor=CENTER)

        antrian.pop(0)
        namapasien.pop(0)
        alamatpasien.pop(0)


def keluar():
    with open("store.txt", 'r') as f:
        a = f.readlines()
    b = int(a[0])
    b = b - b
    with open("store.txt", 'w') as f:
        f.write(str(b))
    exit()


def cekantrian():
    if len(antrian) == 0:
        clear_frame()

        cek_antrian = Label(frame, text="DAFTAR ANTRIAN", font=('Lucida Sans', 16, 'bold'), bg="#F3E2A9")
        cek_antrian.place(relx=0.5, rely=0.4, anchor=CENTER)
        urutan_antrian = Label(frame, text="Tidak Ada Antrian", font=('Lucida Sans', 11), bg="#F3E2A9")
        urutan_antrian.place(relx=0.5, rely=0.47, anchor=CENTER)
        done = Button(frame, text="Selesai", height="1", width="13", bg="#FACC2E", command=menu)
        done.place(relx=0.5, rely=0.6, anchor=CENTER)

    else:
        clear_frame()
        cek_antrian = Label(frame, text="DAFTAR ANTRIAN", font=('Lucida Sans', 15, 'bold'), bg="#F3E2A9")
        cek_antrian.place(relx=0.5, rely=0.4, anchor=CENTER)
        urutan_antrian = Label(frame, text="{}".format(antrian), font=('Lucida Sans', 13), bg="#F3E2A9")
        urutan_antrian.place(relx=0.5, rely=0.5, anchor=CENTER)
        done = Button(frame, text="Selesai", height="1", width="13", bg="#FACC2E", command=menu)
        done.place(relx=0.5, rely=0.6, anchor=CENTER)


def jumlahantrian():
    clear_frame()
    banyakantrian = len(antrian)
    jumlah_antrian = Label(frame, text="JUMLAH ANTRIAN\n {}".format(banyakantrian), font=('Lucida Sans', 15, 'bold'), bg="#F3E2A9")
    jumlah_antrian.place(relx=0.5, rely=0.4, anchor=CENTER)
    done = Button(frame, text="Selesai", bg="#FACC2E", height="1", width="13", command=menu)
    done.place(relx=0.5, rely=0.5, anchor=CENTER)


def menu():
    clear_frame()
    mylabel1 = Label(frame,
                     text="==================================================================\n""|  RUMAH SAKIT  |\n" "==================================================================\n",
                     font=('Helvetica', 14, 'bold'), fg="black", bg="#F3E2A9")
    mylabel1.pack()

    mylabel2 = Label(frame,
                     text="Selamat Datang \n Silahkan Pilih Menu\n",
                     font=('Cambria', 12, 'bold'), fg="black", bg="#F3E2A9")
    mylabel2.pack()

    mybutton1 = Button(frame, text="1. DAFTAR PASIEN\n", height="2", width="16", font=('Cambria', 11),
                       fg="black", bg="#F3E2A9", command=menudaftar)
    mybutton1.pack()

    mybutton2 = Button(frame, text="2. PANGGIL PASIEN\n", height="2", width="16", font=('Cambria', 11),
                       fg="black", bg="#F3E2A9", command=panggilpasien)
    mybutton2.pack()

    mybutton3 = Button(frame, text="3. CEK ANTRIAN \n", height="2", width="16", font=('Cambria', 11),
                       fg="black", bg="#F3E2A9", command=cekantrian)
    mybutton3.pack()

    mybutton4 = Button(frame, text="4. JUMLAH ANTRIAN\n", height="2", width="16", font=('Cambria', 11),
                       fg="black", bg="#F3E2A9", command=jumlahantrian)
    mybutton4.pack()

    mybutton5 = Button(frame, text="5. KELUAR\n", height="2", width="16", font=('Cambria', 11),
                       fg="black", bg="#F3E2A9", command=keluar)
    mybutton5.pack()


root = Tk()
root.resizable(height=False, width=False)
root.iconbitmap("D:\Fahmi Ibrahim\KULIAH\smst3\Struktur Data\Project Queue\Source Code\gambar\logo_rs.ico")
root.title('Antrian Rumah Sakit')
canvas = Canvas(root, height=620, width=400, bg="#F3E2A9")
canvas.pack()
frame = Frame(root, bg="#F3E2A9")
frame.place(relwidth=0.8, relheight=0.8, relx=0.1, rely=0.1)

menu()
root.mainloop(
)
