import os
import queue


class myQueue:

    def __init__(self):
        self.items = queue.Queue()

    # Menambah data ke queue
    def enqueue(self, item):
        self.items.put(item)

    # Mengeluarkan data dari queue
    def dequeue(self):
        if not self.items.empty():
            return self.items.get()
        else:
            return "Tidak ada Antrian"

    # Menghitung panjang queue
    def size(self):
        return self.items.qsize()

    # Agar antrian berurut
    def nomorurut(self):
        with open("store.txt", 'r') as f:
            a = f.readlines()                   # read from file to variable a
        b = int(a[0])                           # get integer at first position
        b = b + 1                               # increment
        with open("store.txt", 'w') as f:       # open same file
            f.write(str(b))                     # writing a assuming it has been changed
        return b

    # Main menu aplikasi
    def mainmenu(self):
        pilih = "y"
        antrian = []
        while pilih == "y":
            os.system("cls")
            print("=========================")
            print("|  Antrian Rumah Sakit  |")
            print("=========================")
            print("1. Daftar Pasien")
            print("2. Panggil Pasien")
            print("3. Cek Antrian")
            print("4. Jumlah Antrian")
            print("5. Keluar Program")
            print("=========================")

            pilihan = str(input("Silakan masukan pilihan anda: "))
            if pilihan == "1":
                os.system("cls")
                obj = input("Masukan Nama Anda: ")
                if not obj == "":
                    nomorantrian = self.nomorurut()
                    self.enqueue(nomorantrian)
                    antrian.append(nomorantrian)
                    print("Terima kasih Bapak/Ibu " + obj + " telah ditambahkan \ndengan nomor Antrian ", nomorantrian)
                    input("")
                else:
                    print("Nama tidak boleh kosong!")
                    input("")
            elif pilihan == "2":
                os.system("cls")
                temp = self.dequeue()
                if temp != "Tidak ada Antrian":
                    print("=====Panggilan Antrian=====")
                    print("Nomor Antrian: ", temp)
                    antrian.pop(0)
                else:
                    print("Antrian kosong")
                input("")
            elif pilihan == "3":
                os.system("cls")
                if not antrian == []:
                    print(antrian)
                    input("")
                else:
                    print("Tidak ada Antrian")
                    input("")
            elif pilihan == "4":
                os.system("cls")
                print("Panjang Antrian: " + str(self.size()))
                input("")
            elif pilihan == "5":
                if input("Press Enter to Exit...") == "":
                    with open("store.txt", 'r') as f:
                        a = f.readlines()
                    b = int(a[0])
                    b = b - b
                    with open("store.txt", 'w') as f:
                        f.write(str(b))
                    exit()
            else:
                continue


if __name__ == "__main__":
    q = myQueue()
    q.mainmenu()


